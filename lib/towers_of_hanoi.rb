# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    while not self.won?

      @towers
      puts "The towers are labeled 0, 1, and 2."
      puts "Please enter first the tower from which you'll take a disc."
      puts "Answer e for each answer to exit."
      from = gets.chomp
      to = gets.chomp
      if from == "e" && to == "e"
        break
      else
        self.move(from.to_i, to.to_i)
      end

    end
    self.won? ? "You've won!" : "Please try again soon!"
  end

  def move(from_tower, to_tower)
    if valid_move?(from_tower, to_tower)
      @towers[to_tower] << @towers[from_tower].pop
    else
      return "That movement is invalid. Please pick again."
    end
  end

  def valid_move?(from_tower, to_tower)

    # Can't pick a tower outside of the range
    if from_tower > 2 || to_tower > 2
      return false
    end
    # Check if from_tower is empty
    if @towers[from_tower].empty?
      return false
    end

    # Check to make sure no disc is moved onto a larger one
    # The first statement is used to make sure we don't compare a nil
    # to a Fixnum
    if @towers[to_tower].empty? || @towers[from_tower].last < @towers[to_tower].last
      return true
    else
      return false
    end
  end

  def won?

    # Can only win when all three discs have been placed onto towers 1,2
    @towers[1].size == 3 || @towers[2].size == 3
  end

end
